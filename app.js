// Requiring node modules and instantiating essential objects
require('node-env-file')('env');
var express = require('express');
var app = express();
var mongo = require('mongodb');
var monk = require('monk');
var http = require('http')
var server = http.createServer(app);
var io = require('socket.io')(server);
var _ = require('underscore');

// Configuring app to use static files
app.use('/res', express.static(__dirname + '/res'));

// Setting variables
var TAG = 'server';
var PORT = process.env.PORT;
var LOG = process.env.LOG == 'true';

var DB = monk(process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME, {
  username : process.env.DB_USERNAME,
  password : process.env.DB_PASSWORD
});

function log(tag, data, json) {
  if(LOG) {
    console.log(tag, ': ' + data);
      if(!_.isUndefined(json) && !_.isNull(json)) {
        console.log('........................................');
        console.log(JSON.stringify(json, null, 2));
      }
    console.log('----------------------------------------');
  }
}

function time() {
  log(TAG, 'time');
  return new Date().getTime()
}

function isPothole(x, y, z, ff, us) {
  log(TAG, 'isPothole');
	var TX = 800, TY = 800, TZ = 1150; // accelerometer threshold values
	var TUS = 6; // ultrasonic threshold value (in inches)
	if(ff == 1) {
		return true;
	}
	if(us >= TUS) {
		return true;
	}
	if(z >= TZ) {
		return true;
	}
	return false;
}

// Listening on a port
server.listen(PORT, function(){
  log(TAG, 'Listening on port ' + PORT);
});

// Setting routes
app.get('/', function(req, res) {
  log(TAG, 'get -> /');
  res.sendFile(__dirname + '/index.html');
});

app.get('/add_data', function(req, res) {
  log(TAG, 'get -> /add_data');
  var si = req.param('sensor_id'),
  x = parseFloat(req.param('acc_x')),
  y = parseFloat(req.param('acc_y')),
  z = parseFloat(req.param('acc_z')),
  ff = req.param('free_fall'),
  us = parseFloat(req.param('ultrasonic_dist')),
  cx = parseFloat(req.param('coord_x')),
  cy = parseFloat(req.param('coord_y')),
  t = time();
  var p = isPothole(x, y, z, ff, us);
  DB.get('data').insert({
  	'sensor_id' : si,
	'acc_x' : x,
	'acc_y' : y,
	'acc_z' : z,
	'free_fall' : ff,
	'ultrasonic_dist' : us,
	'coord_x' : cx,
	'coord_y' : cy,
	'timestamp' : t,
	'pothole' : p
  }, function(err, doc) {
  	if(err == null) {
  		res.json({
  			'success' : true,
  			'data' : doc
  		});
  		io.emit('data', doc);
  	} else {
  		log(TAG, err);
  		res.json({
  			'success' : false
  		});
  	}
  });
});

io.on('connect', function(socket) {

  log(TAG, 'Connected to socket ' + socket.id);

  socket.on('disconnect', function() {
    log(TAG, 'Disconnected from socket ' + socket.id);
  });

  socket.on('get_voids', function(data) {
  	log(TAG, 'get_voids');
	DB.get('data').find({
	  	'pothole' : true
	  }, { sort : { 'timestamp' : -1 } },  function(err, docs) {
	  	if(err == null) {
	  		socket.emit('get_voids', {
	  			'success' : true,
	  			'potholes' : docs
	  		});
	  	} else {
	  		log(TAG, err);
	  		socket.emit('get_voids', {
	  			'success' : false
	  		});
	  	}
  	});
  });

});